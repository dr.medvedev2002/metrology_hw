#!/usr/bin/env python

import PySimpleGUI as sg
from hw_math import Hw_math
from pathlib import Path
import matplotlib.pyplot as plt
import PySimpleGUI as sg
import os.path
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np

a = Hw_math()
sg.theme("Reddit")
flag = False
# First the window layout in 2 columns


def get_separator_value(window: sg.Window): #move it for check button
    if window['comma separator'].get() == True:
        sep = ','
    elif window["newline separator"].get() == True:
        sep = '\n'
    else:
        sep = " "
    return sep

def print_list(lst: list, sep):
    return sep.join(map(str,lst))

def plot_fig():
    fig = plt.figure(dpi=100)
    x_j_up, x_j_mid = a.get_x_list()
    x = x_j_mid #сюда X_j_0
    y = a.get_y_list()
    fig.add_subplot(111).plot(x,y, color='red')
    plt.xlabel("x_j_0")
    plt.ylabel("n_j")
    return fig

def delete_figure_agg(figure_agg):
    figure_agg.get_tk_widget().forget()
    plt.close('all')

def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

file_list_column = [
    [
        sg.Text("Input file"),
        sg.In(size=(25, 1), enable_events=True, key="-FILE-"),
        sg.FileBrowse(),
        sg.Button("Read")
    ],
    [
        sg.Text("Separator"),
        sg.Radio("Commas", "Radio1", default=False, key="comma separator"),
        sg.Radio("Space", "Radio1", default=False, key="space separator"),
        sg.Radio('\\n', "Radio1", default=True, key="newline separator"),
    ],
    [
        sg.Multiline(
            default_text="", enable_events=True, size=(40, 20), key="-DATA LIST-"
        ),
        sg.Button("Check", key="-CHECK-")
    ],
]

# For now will only show the name of the file that was chosen
second_list_column_top = [
    [sg.Text(f"average: "), sg.Text(a.avg, key="-average-")],
    [sg.Text(f"S: ") ,sg.Text(a.S, key="-S-")],
    [sg.Text(f"Charlie crit value: "), sg.Text(a.avg, key="-charlie-")],
    [sg.Text('Excluded values: '), sg.Text('', key="excluded")],
]
second_list_column_bottom = [
    [sg.Canvas(key='-CANVAS-', size=(300,200), pad=(15,15))]
]

second_list_column = [
    [sg.Frame(layout=second_list_column_top,title="Results", vertical_alignment="top")],
    [sg.Frame(layout=second_list_column_bottom, title="Graph")],
]

# ----- Full layout -----
layout = [
    [
        sg.Column(file_list_column),
        sg.VSeperator(),
        sg.Column(second_list_column,key="Result Column", visible=False),
    ]
]


window = sg.Window("Charlie misses", layout)

# Run the Event Loop
while True:
    event, values = window.read()
    if event == "Exit" or event == sg.WIN_CLOSED:
        break
    if event == "Read":
        filename = values['-FILE-']
        if Path(filename).is_file():
            try:
                with open(filename, "rt", encoding='utf-8') as f:
                    text = f.read()
                    a.set_data(text, get_separator_value(window))
                    window['-DATA LIST-'].update(print_list(a.get_data(),get_separator_value(window)))

            except Exception as e:
                window['-DATA LIST-'].update(f"Are you use right separator?\nERROR: {e}")

    if event == "-CHECK-":
        if  window['-DATA LIST-'].get():
            print('hello')
            print(window['-DATA LIST-'].get())
            a.set_data(window['-DATA LIST-'].get(), get_separator_value(window))

        if  a.get_data():
            window['Result Column'].update(visible=True)
            window['-average-'].update(a.average())
            window['-S-'].update(a.std())
            misses, not_misses, sharl_crit, size_flag = a.check_misses()
            window['-charlie-'].update(sharl_crit)
            if flag:
                delete_figure_agg(figure_canvas_agg)
            figure_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, plot_fig())
            if size_flag:
                window["-DATA LIST-"].update(f"Sample size less than 20 \n{print_list(a.get_data(), get_separator_value(window))}")
            window["-DATA LIST-"].update(print_list(not_misses, get_separator_value(window)))
            if misses:
                window['excluded'].update(print_list(misses, get_separator_value(window)))
            else:
                window['excluded'].update("No misses in measurement results ")
            flag = True
            



window.close()
