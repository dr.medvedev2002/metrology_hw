import numpy as np
import math

class Hw_math:
    def __init__(self):
        self.__data = []
        self.S = 0
        self.avg = 0
        self.K = 0 


    def get_data(self):
        a = self.__data
        return a

    def set_data(self, ui_data:str, separator=" "):
        self.__data = []
        if separator != ",":
            ui_data = ui_data.replace(",",".")
        if separator == '\n':
            data = ui_data.splitlines()
            for i in data:
                self.__data.append(float(i.strip()))
        else:
            ui_data = ui_data.split(separator)
            for i in ui_data:
                self.__data.append(float(i.strip()))
        return True

        
    # def from_list(self,path, separator=" "):
    #     with open(path, 'r') as f:
    #         file_data = f.read()

    #     file_data = file_data.split(separator)
    #     for i in file_data:
    #         self.__data.append(float(i.strip()))
    #     return True
        
    def average(self):
        if self.__data: 
            self.avg = sum(self.__data)/len(self.__data)
            return self.avg
        else:
            return 0

    def std(self):
        self.S = np.std(self.__data)
        return np.std(self.__data)

    def get_len(self):
        return len(self.__data)

    def get_x_list(self):
        x_j_up = []
        x_j_mid = []
        interval_width = (max(self.__data) - min(self.__data)) / math.ceil(math.sqrt(self.get_len()))
        for i in range(1,math.ceil(math.sqrt(self.get_len()))+1):
            x_j_up.append(min(self.__data) + interval_width*i)
            x_j_mid.append(min(self.__data) + interval_width*i*0.5)
        
        self.x_j_up = x_j_up
        self.x_j_mid = x_j_mid

        return x_j_up, x_j_mid
    
    def get_y_list(self):
        y = []
        for i in range(len(self.x_j_up)):
            cnt = 0
            if i == 0:
                for j in self.__data:
                    if j < self.x_j_up[i]:
                        cnt += 1
                y.append(cnt)
            else:
                for j in self.__data:
                    if j < self.x_j_up[i] and j >= self.x_j_up[i-1]:
                        cnt += 1
                y.append(cnt)
        
        self.y = y
        return y
                


    def check_misses(self):
        not_misses = []
        size_flag = False
        misses = []
        self.K = (0.212204493 + 0.897158545*np.log(len(self.__data)))/(1+0.1815170415*np.log(len(self.__data)) - 0.00715123*(np.log(len(self.__data)))**2)
        if len(self.__data) >= 20:
            for i in self.__data:
                if abs(i - self.avg) < self.K * self.S:
                    not_misses.append(i)
                else:
                    misses.append(i)
        else:
            size_flag = True
        
        self.__data = not_misses
        return misses, not_misses, self.K, size_flag

    
